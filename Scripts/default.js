﻿/// <reference path='_references.js' />
var greeting = 'Hello World';
var wsUri = 'ws://echo.websocket.org/';
var webSocket;
var timerId = 0;

function writeOutput(message) {
    var output = $('#divOutput');
    output.html(output.html() + '<br/>' + message);
}

function checkSupported() {
    if (window.WebSocket) {
        writeOutput('Websockets supported!');
        return true;
    } else {
        writeOutput('WebSockets NOT supported');
        $('#btnSend').attr('disabled', 'disabled');
        return false;
    }
}

function keepAlive() {
    var timeout = 15000;
    if (webSocket.readyState == WebSocket.OPEN) {
        webSocket.send('');
        writeOutput("sent keepalive");
    }
    timerId = setTimeout(keepAlive, timeout);
}

function cancelKeepAlive() {
    if (timerId) {
        cancelTimeout(timerId);
    }
}

function connect() {
    webSocket = new WebSocket(wsUri);
    webSocket.onopen = function (evt) { onOpen(evt) };
    webSocket.onclose = function (evt) { onClose(evt) };
    webSocket.onmessage = function (evt) { onMessage(evt) };
    webSocket.onerror = function (evt) { onError(evt) };
}

function doSend() {
    if (webSocket.readyState != WebSocket.OPEN) {
        writeOutput('NOT OPEN: ' + $('#txtMessage').val());
        return;
    }
    writeOutput("SENT:" + $('#txtMessage').val());
    webSocket.send($('#txtMessage').val());
}

function onOpen(evt) {
    writeOutput('CONNECTED');
    keepAlive();
}

function onClose(evt) {
    cancelKeepAlive();
    writeOutput('DISCONNECTED');
}

function onMessage(evt) {
    writeOutput('RESPONSE:' + evt.data);
}

function onError(evt) {
    writeOutput('ERROR' + evt.data);
}


function areaOfPizzaSlice(diameter, slicesPerPizza) {
    var pizzaParts = new Array();
    //pizzaParts[0] = 'Peperoni';
    // alert(pizzaParts[1]);

    return areaOfPizza(diameter) / slicesPerPizza;
    
    function areaOfPizza(diameter) {
        var radius = diameter / 2;
        return 3.141592 * radius * radius;
    }
    
}

function addNumbersNonAjax() {
    var x = document.getElementById('x').value;
    var y = document.getElementById('y').value;
    var result = document.getElementById('result');
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var jsonObject = JSON.parse(xmlhttp.response);
            result.innerHTML = jsonObject.result;
        }
    }
    xmlhttp.addEventListener("progress", updateProgress, false);
    xmlhttp.open("GET", "http://localhost:8080/addition?x=" + x + "&y=" + y, true);
    xmlhttp.send();
}


function displayResult(serverData) {
    $('#result').html(serverData.result);
}


function serverAddition(data) {
    return $.getJSON('http://localhost:8080/addition', data);
}


function getFormData() {
    var x = $('#x').val();
    var y = $('#y').val();
    return { "x": x, "y": y };
}


function addNumbers() {
    var data = getFormData();
    serverAddition(data).done(displayResult);
}


function updateProgress(evt) {
    if (evt.lengthComputable) {
        var percentComplete = evt.loaded / evt.total;
    }
}

function timeoutAsync(milliseconds) {
    var deferred = $.Deferred();
    setTimeout(function () { deferred.resolve(); }, milliseconds);
    return deferred.promise();
}

